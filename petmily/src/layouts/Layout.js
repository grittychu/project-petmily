import Header from "../components/Header";
import Navbar from "../components/Navbar";
import { Outlet } from "react-router-dom";

function Layout() {
  const fixStyle = {
    position: "absolute",
    width: "100%",
    height: "50px",
    top: "0px",
    left: "0px",
    margin: "0px",
    display: "flex",
    alignItems: "center",
    color: "white",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    padding: "8px 12px",
    justifyContent: "space-between",
  };
  return (
    <>
      <div style={fixStyle}>
        <Header />
        <Navbar />
      </div>
      <Outlet />
    </>
  );
}

export default Layout;
